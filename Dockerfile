FROM openjdk:8-jdk-alpine
COPY target/formacion-0.0.1-SNAPSHOT.jar app3.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/app3.jar"]