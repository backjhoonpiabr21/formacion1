package com.example.formacion;


import net.bbva.Calculadora;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {
    @RequestMapping("/saludo")
    public String index() {
        Calculadora calculadora = new Calculadora();
        double resulado = calculadora.obtenerPrecioFinal(100);
        return "Hola, soy tu Api y el precio final de 100 es " + String.valueOf(resulado);
    }
}

