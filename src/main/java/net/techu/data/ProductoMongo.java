package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("productosJHOOnpi")
public class ProductoMongo {
    public String id;
    public String nombre;
    public Double precio;

    public ProductoMongo() {
    }
    public ProductoMongo(String nombre, Double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }
    @Override
    public String toString() {
        return String.format("Producto [id=%s, nombre=%s, precio=%s]", id, nombre, precio);
    }
}
