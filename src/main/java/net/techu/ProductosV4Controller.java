package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller  {

    @Autowired
    private ProductoRepository repository;


    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }
    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        System.out.println("Estoy en añadir Mongo" + minimo);
        System.out.println("Estoy en añadir Mongo" + maximo);
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces="application/json")
    public ResponseEntity<String> addProductoMongo(@RequestBody Producto productoNuevo) {

        System.out.println("Estoy en añadir Mongo");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        repository.insert(new ProductoMongo(productoNuevo.getNombre(),productoNuevo.getPrecio()));

        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
//    @PutMapping("/v4/productos")
//    public ResponseEntity<String> updateProductoMongo(@RequestBody Producto cambios) {
//        ResponseEntity<String> resultado = null;
//        try {
//            List<ProductoMongo> productoAModificar = (List<ProductoMongo>) repository.findByNombre(cambios.getNombre());
//            System.out.println("Voy a modificar el producto" + String.valueOf(cambios.getId()));
//           // System.out.println("Precio actual: " + String.valueOf(productoAModificar);
//            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
//           // productoAModificar.setNombre(cambios.getNombre());
//           // productoAModificar.setPrecio(cambios.getPrecio());
//            repository.save(new ProductoMongo(cambios.getNombre(), cambios.getPrecio()));
//            //      listaProductos.set(cambios.getId(), productoAModificar);
//            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        } catch (Exception ex) {
//            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
//        }
//        return resultado;
//
//    }
    @PutMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo)
    {
        Optional<ProductoMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
      //     productoAModificar.color = productoMongo.color;
            productoAModificar.precio = productoMongo.precio;
        }
        ProductoMongo guardado = repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        /* Posibilidad de buscar el producto, después eliminarlo si existe */
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}


