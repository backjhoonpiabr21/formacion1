package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@RestController
public class ProductosController {
    private ArrayList<String> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }
    @RequestMapping(value = "/productos", method = RequestMethod.GET, produces = "application/json")
    public  ArrayList<String> obtenerLsitado()
    {
        System.out.println("Estoy en obtener");
        return listaProductos;

    }

     @RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity<String > respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = "No se ha encontrado el producto";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    @RequestMapping( value = "/techu/productos", method = RequestMethod.POST, produces = "application/json")
    public void AddProducto()
    {
        System.out.println("Estoy en añadir");
        listaProductos.add("NUEVO");
    }

    @RequestMapping(value = "/techu/productos/{nom}/{cat}", method = RequestMethod.POST, produces = "application/json")
    public void AddProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria)
    {
        System.out.println("Voy añadir el nombre " + nombre + " y categoria" + categoria);
        listaProductos.add(nombre);
    }
    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAEliminar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}
